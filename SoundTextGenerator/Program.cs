﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace SoundTextGenerator {
  class Program {
    private static Dictionary<char, ushort> df;

    static void Main(string[] args) {
      InitializeFreq();

      if (args.Length >= 3) {
        if(args[0] != null) {
          if(args[1] != null) {
            if (int.Parse(args[1]) <= 65535) {
              if(File.Exists(args[0])) {
                PlayMusic(TextProcessor.ProcessFile(args[0]), ushort.Parse(args[1]));
              }
            }
          }
        }
      }
      Console.WriteLine("Playback complete.");
      Console.ReadLine();
    }

    static void PlayMusic(string s, ushort bpm) {
      foreach (char c in s) {
        if (c == '\n') {
          Thread.Sleep(1000);
        }
        if (c == ';') {
          Thread.Sleep(250);
        }
        if (c == ',') {
          Thread.Sleep(100);
        }
        if (c == ' ') {
          Thread.Sleep(50);
        }

        if (df.ContainsKey(c)) {
          Sound.Generate(df[c], bpm+100, 65535);
        }

        Thread.Sleep(bpm);
      }
    }

    static void InitializeFreq() {
      df = new Dictionary<char, ushort>();

      // ------------------- OCTAVE 4 START
      df.Add('a', 261);
      df.Add('ą', 277);
      df.Add('b', 293);
      df.Add('c', 311);
      df.Add('ć', 329);
      df.Add('d', 349);
      df.Add('e', 369);
      df.Add('ę', 392);
      df.Add('f', 415);
      df.Add('g', 440);
      df.Add('h', 466);
      df.Add('i', 494);
      // ------------------- OCTAVE 4 END
    
      // ------------------- OCTAVE 3 START
      df.Add('j', 130);
      df.Add('k', 138);
      df.Add('l', 146);
      df.Add('ł', 155);
      df.Add('m', 165);
      df.Add('n', 174);
      df.Add('ń', 185);
      df.Add('o', 196);
      df.Add('ó', 207);
      df.Add('p', 220);
      df.Add('q', 233);
      df.Add('r', 245);
      // ------------------- OCTAVE 3 END

      // ------------------- OCTAVE 2 START
      df.Add('s', 65);
      df.Add('ś', 69);
      df.Add('t', 73);
      df.Add('u', 77);
      df.Add('v', 82);
      df.Add('w', 87); // FIXME: missing F# (94?)
      df.Add('x', 98);
      df.Add('y', 103);
      df.Add('z', 110);
      df.Add('ź', 116);
      df.Add('ż', 123);
      // ------------------- OCTAVE 2 END

      // ------------------- OCTAVE 6 START
      df.Add('1', 1046);
      df.Add('2', 1108);
      df.Add('3', 1174);
      df.Add('4', 1244);
      df.Add('5', 1318);
      df.Add('6', 1397);
      df.Add('7', 1480);
      df.Add('8', 1568);
      df.Add('9', 1661);
      df.Add('0', 1760);
      df.Add('!', 1864);
      df.Add('.', 1975);
    }
  }
}
