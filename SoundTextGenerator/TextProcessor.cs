﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SoundTextGenerator {
  static class TextProcessor {
    public static string ProcessFile(string fileName) {
      if (!File.Exists(fileName)) return null;

      string s;
      
      using (StreamReader sr = new StreamReader(fileName)) {
        s = sr.ReadToEnd();
      }

      return s;
    }
  }
}
